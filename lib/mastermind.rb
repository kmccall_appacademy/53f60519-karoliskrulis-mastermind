require 'byebug'

# debugger
class Code

  PEGS = { "B" => :blue,
           "G" => :green,
           "O" => :orange,
           "P" => :purple,
           "R" => :red,
           "Y" => :yellow }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    # string = string.upcase.split('')
    # raise "must be 4 units!" unless string.length == 4
    # raise "invalid characters" unless string.all? { |s| PEGS.key?(s) }
    # Code.new(string.join)
    # none of this works because it evaluates toa string (?)
    pegs = string.split("").map do |letter|
      raise "parse error" unless PEGS.has_key?(letter.upcase)
      PEGS[letter.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    secret = []
    4.times { secret << PEGS.keys[rand(0..5)] }

    Code.new(secret)
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(compare)
    exact = 0
    pegs.each_index do |i|
      exact += 1 if pegs[i] == compare[i]
    end
    exact
  end

  def near_matches(compare)
    near = 0
    pegs.each_index do |i|
      if pegs[i] != compare[i] && pegs[i..pegs.length].include?(compare[i])
        near += 1
      end
    end
    near
  end

  def ==(compare)
    return false unless compare.is_a?(Code)
    pegs == compare.pegs
  end

end



class Game
  attr_reader :secret_code
  attr_accessor :guess

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @guess = guess
  end

  def get_guess
    Code.parse(gets.chomp)
  end

  def display_matches(guess)
    exact = @secret_code.exact_matches(guess)
    near = @secret_code.near_matches(guess)
    puts "exact matches: #{exact}"
    puts "near matches: #{near}"
  end

end

# if __FILE__ == $PROGRAM_NAME
#   new_game = Game.new
#   new_game.play
# end
